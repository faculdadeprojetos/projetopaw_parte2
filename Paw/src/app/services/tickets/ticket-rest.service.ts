import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ticket } from 'src/app/models/ticket';

const endpoint = 'http://localhost:5000/store/ticket/';

@Injectable({
  providedIn: 'root',
})
export class TicketRestService {
  constructor(private http: HttpClient) {}

  getTicketsForEvent(eventName: string): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(
      endpoint + 'getTicketsForEvent/' + eventName
    );
  }
}
