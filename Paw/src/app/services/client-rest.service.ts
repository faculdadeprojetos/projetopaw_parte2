import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../models/client';
import { AuthService } from './auth/auth.service';
import { FormClient } from '../models/formClient';

//import { ClientPurchase } from '../models/clientPurchase';

const endpoint = 'http://localhost:5000/store/client';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class ClientRestService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  /**
   * Get a Client
   * @returns
   */
  getClient(): Observable<Client> {
    return this.http.get<Client>(endpoint + '/getClient');
  }

  /*
   * Registration of a client
   */
  createClient(client: FormClient): Observable<Client> {
    return this.http.post<Client>(endpoint + '/register', client, httpOptions);
  }

  /*
   * Edit of a client
   */
  editClient(client: FormClient): Observable<Client> {
    return this.http.put<Client>(
      endpoint + `/updateClient`,
      client,
      httpOptions
    );
  }

  /**
   * Returns the list of purchases made by the logged client
   */
  /**getPurchasesOfActualClient(): Observable<ClientPurchase[]> {
    return this.http.post<ClientPurchase[]>(
      endpoint + 'sales/getSalesByToken',
      JSON.stringify({ token: this.auth.getToken() }),
      httpOptions
    );
  }**/

  /**
   * Returns the path of the invoice document
   */
  //getPurchaseInvoice(docId: number): Observable<any> {
  // return this.http.post<any>(
  //  endpoint + 'purchases/generatePdfCredit',
  //  JSON.stringify({ token: this.auth.getToken(), docId: docId }),
  //   invoiceOptions
  //  );
  // }
}
