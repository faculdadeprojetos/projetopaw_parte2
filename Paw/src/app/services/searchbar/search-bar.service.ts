import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root',
})
export class SearchBarService {
  public searchText = new BehaviorSubject<string>('');

  constructor() {}
}
