import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Event } from '../models/event';

const endpoint = 'http://localhost:5000/store/event/'; // API endpoint for events
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json', // Set request header to indicate JSON content
  }),
};

@Injectable({
  providedIn: 'root',
})
export class EventRestService {
  constructor(private http: HttpClient) {} // Inject HttpClient service for making HTTP requests

  // Fetches events from the API
  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(endpoint + 'getAllEvents'); // Send a GET request to retrieve all events
  }

  getEventByName(name: string | null): Observable<Event> {
    return this.http.get<Event>(endpoint + 'getEventByName/' + name);
  }

  getRelatedEvents(name: string): Observable<string[]> {
    return this.http.get<string[]>(endpoint + 'getRelatedEvents/' + name);
  }

  getClosestEventsToToday(): Observable<Event[]> {
    return this.http.get<Event[]>(endpoint + 'getClosestEventsToToday');
  }
}
