import { TestBed } from '@angular/core/testing';

import { PointsRestService } from './points-rest.service';

describe('PointsRestService', () => {
  let service: PointsRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PointsRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
