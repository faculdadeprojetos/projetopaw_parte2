// Imports for Angular features
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Custom imports
import { HomepageComponent } from './components/homepage/homepage.component';
import { PatrimoniesComponent } from './components/patrimony/patrimonies/patrimonies.component';
import { PersonalInformationComponent } from './components/client-items/profile-items/personal-information/personal-information.component';
import { LoginComponent } from './components/client-items/login/login.component';
import { RegistrationComponent } from './components/client-items/registration/registration.component';
import { AuthGuard } from './services/auth/auth.guard';
import { HomepageDetailsComponent } from './components/homepage/homepage-details/homepage-details.component';
import { CheckoutComponent } from './components/checkout-items/checkout/checkout.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { AfterCheckoutComponent } from './components/checkout-items/after-checkout/after-checkout.component';

const routes: Routes = [
  { path: '', component: HomepageComponent }, // Define a route for the homepage component

  { path: 'events/:name', component: HomepageDetailsComponent },

  { path: 'about-us', component: AboutUsComponent },

  { path: 'login', component: LoginComponent }, // Define a route for the login component

  { path: 'patrimonies', component: PatrimoniesComponent },

  { path: 'checkout/after-checkout', component: AfterCheckoutComponent },

  {
    path: 'clients',
    component: PersonalInformationComponent,
    canActivate: [AuthGuard],
  },

  { path: 'clients/registration', component: RegistrationComponent },

  { path: 'checkout', component: CheckoutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], // Configure the router module with the defined routes
  exports: [RouterModule], // Export the configured router module for other modules to use
})
export class AppRoutingModule {}
