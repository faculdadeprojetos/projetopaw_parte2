import { NgModule } from '@angular/core'; // Importing NgModule from Angular Core
import { BrowserModule } from '@angular/platform-browser'; // Importing BrowserModule from Angular Platform Browser
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // Importing BrowserAnimationsModule for animations
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // Importing FormsModule and ReactiveFormsModule for forms
import { AppRoutingModule } from './app-routing.module'; // Importing AppRoutingModule for routing
//import { JwtModule } from "@auth0/angular-jwt"; // Importing JwtModule for JWT functionality
import { JwtInterceptor } from './shared/jwt.interceptor'; // Importing JwtInterceptor for JWT Interceptor functionality
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'; // Importing HttpClientModule and HTTP_INTERCEPTORS for Http operations
import { NgxPaginationModule } from 'ngx-pagination'; // Importing NgxPaginationModule for pagination
//import {NgxQRCodeModule} from '@techiediaries/ngx-qrcode' // Importing NgxQRCodeModule for QR Code generation
import { QRCodeModule } from 'angularx-qrcode'; // Importing QRCodeModule for QR Code generation

// Components
import { AppComponent } from './app.component'; // Importing AppComponent
import { HeaderComponent } from './components/partials/header/header.component'; // Importing HeaderComponent
import { FooterComponent } from './components/partials/footer/footer.component'; // Importing FooterComponent
import { HomepageComponent } from './components/homepage/homepage.component'; // Importing HomepageComponent
import { HomepageDetailsComponent } from './components/homepage/homepage-details/homepage-details.component'; // Importing HomepageDetailsComponent
import { PatrimoniesComponent } from './components/patrimony/patrimonies/patrimonies.component';
import { PatrimoniesDetailComponent } from './components/patrimony/patrimonies/patrimonies-detail/patrimonies-detail.component';
import { LoginComponent } from './components/client-items/login/login.component';
import { PersonalInformationComponent } from './components/client-items/profile-items/personal-information/personal-information.component';
import { EditProfileComponent } from './components/client-items/profile-items/edit-profile/edit-profile.component';
import { RegistrationComponent } from './components/client-items/registration/registration.component';
import { SearchBarFilterPipe } from './shared/searchBarFilter.pipe';

// Material Form Controls
import { MatAutocompleteModule } from '@angular/material/autocomplete'; // Importing MatAutocompleteModule for autocomplete
import { MatCheckboxModule } from '@angular/material/checkbox'; // Importing MatCheckboxModule for checkboxes
import { MatDatepickerModule } from '@angular/material/datepicker'; // Importing MatDatepickerModule for datepicker
import { MatFormFieldModule } from '@angular/material/form-field'; // Importing MatFormFieldModule for form fields
import { MatInputModule } from '@angular/material/input'; // Importing MatInputModule for input fields
import { MatRadioModule } from '@angular/material/radio'; // Importing MatRadioModule for radio buttons
import { MatSelectModule } from '@angular/material/select'; // Importing MatSelectModule for select dropdowns
import { MatSliderModule } from '@angular/material/slider'; // Importing MatSliderModule for sliders
import { MatSlideToggleModule } from '@angular/material/slide-toggle'; // Importing MatSlideToggleModule for slide toggles

// Material Navigation
import { MatMenuModule } from '@angular/material/menu'; // Importing MatMenuModule for menus
import { MatSidenavModule } from '@angular/material/sidenav'; // Importing MatSidenavModule for sidenav
import { MatToolbarModule } from '@angular/material/toolbar'; // Importing MatToolbarModule for toolbar

// Material Layout
import { MatCardModule } from '@angular/material/card'; // Importing MatCardModule for cards
import { MatDividerModule } from '@angular/material/divider'; // Importing MatDividerModule for dividers
import { MatExpansionModule } from '@angular/material/expansion'; // Importing MatExpansionModule for expansions
import { MatGridListModule } from '@angular/material/grid-list'; // Importing MatGridListModule for grid lists
import { MatListModule } from '@angular/material/list'; // Importing MatListModule for lists
import { MatStepperModule } from '@angular/material/stepper'; // Importing MatStepperModule for stepper
import { MatTabsModule } from '@angular/material/tabs'; // Importing MatTabsModule for tabs
import { MatTreeModule } from '@angular/material/tree'; // Importing MatTreeModule for tree view

// Material Buttons & Indicators
import { MatButtonModule } from '@angular/material/button'; // Importing MatButtonModule for buttons
import { MatButtonToggleModule } from '@angular/material/button-toggle'; // Importing MatButtonToggleModule for button toggles
import { MatBadgeModule } from '@angular/material/badge'; // Importing MatBadgeModule for badges
import { MatChipsModule } from '@angular/material/chips'; // Importing MatChipsModule for chips
import { MatIconModule } from '@angular/material/icon'; // Importing MatIconModule for icons
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'; // Importing MatProgressSpinnerModule for progress spinners
import { MatProgressBarModule } from '@angular/material/progress-bar'; // Importing MatProgressBarModule for progress bars
import { MatRippleModule } from '@angular/material/core'; // Importing MatRippleModule for ripple effects

// Material Popups & Modals
import { MatBottomSheetModule } from '@angular/material/bottom-sheet'; // Importing MatBottomSheetModule for bottom sheets
import { MatDialogModule } from '@angular/material/dialog'; // Importing MatDialogModule for dialogs
import { MatSnackBarModule } from '@angular/material/snack-bar'; // Importing MatSnackBarModule for snack bars
import { MatTooltipModule } from '@angular/material/tooltip'; // Importing MatTooltipModule for tooltips

// Material Data tables
import { MatPaginatorModule } from '@angular/material/paginator'; // Importing MatPaginatorModule for pagination in data tables
import { MatSortModule } from '@angular/material/sort'; // Importing MatSortModule for sorting in data tables
import { MatTableModule } from '@angular/material/table';
import { CheckoutComponent } from './components/checkout-items/checkout/checkout.component';
import { ConfirmationDialogComponent } from './components/checkout-items/confirmation-dialog/confirmation-dialog.component';
import { AfterCheckoutComponent } from './components/checkout-items/after-checkout/after-checkout.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { SeeSalesComponent } from './components/client-items/profile-items/see-sales/see-sales.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    HomepageDetailsComponent,
    HeaderComponent,
    FooterComponent,
    PatrimoniesComponent,
    PatrimoniesDetailComponent,
    LoginComponent,
    RegistrationComponent,
    PersonalInformationComponent,
    EditProfileComponent,
    SearchBarFilterPipe,
    CheckoutComponent,
    ConfirmationDialogComponent,
    ConfirmationDialogComponent,
    AfterCheckoutComponent,
    AboutUsComponent,
    SeeSalesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    //JwtModule,
    NgxPaginationModule,
    QRCodeModule,
    //NgxQRCodeModule, //DEPRECATED

    // Material
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatListModule,
    MatStepperModule,
    MatTabsModule,
    MatTreeModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatRippleModule,
    MatBottomSheetModule,
    MatDialogModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    [HomepageComponent, CheckoutComponent],
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
