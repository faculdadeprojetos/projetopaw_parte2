import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService) {}

  // Intercepts HTTP requests and attaches JWT token to the headers if the user is logged in
  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (this.auth.isLoggedIn()) {
      // Check if the user is logged in
      let token = this.auth.getToken(); // Get the JWT token from AuthService
      request = request.clone({
        setHeaders: {
          'x-access-token': `${token}`, // Set the 'x-access-token' header with the JWT token
        },
      });
    }

    return next.handle(request || null); // Proceed with the intercepted request
  }
}
