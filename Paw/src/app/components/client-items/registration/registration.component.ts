import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';
import { ClientRestService } from 'src/app/services/client-rest.service';
import { FormClient } from 'src/app/models/formClient';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  @Input() client: FormClient;

  //flags to components
  error1 = false;
  error2 = false;
  error3 = false;

  clientForm!: FormGroup;

  constructor(
    private clientService: ClientRestService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: NgToastService
  ) {
    this.client = new FormClient();
  }

  ngOnInit(): void {
    this.clientForm = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(60),
      ]),
      email: new FormControl('', [Validators.required]),
      postalCode: new FormControl('', [Validators.required]),
      cellPhone: new FormControl('', [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9),
      ]),
      dob: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      address: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(30),
      ]),
      nif: new FormControl('', [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9),
      ]),
      city: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }
  get name() {
    return this.clientForm.get('name')!;
  }
  get email() {
    return this.clientForm.get('email')!;
  }
  get postalCode() {
    return this.clientForm.get('postalCode')!;
  }
  get cellPhone() {
    return this.clientForm.get('cellPhone')!;
  }
  get address() {
    return this.clientForm.get('address')!;
  }
  get city() {
    return this.clientForm.get('city')!;
  }
  get password() {
    return this.clientForm.get('password')!;
  }
  get dob() {
    return this.clientForm.get('dob')!;
  }
  get gender() {
    return this.clientForm.get('gender')!;
  }
  get nif() {
    return this.clientForm.get('nif')!;
  }

  /*
   * Registration of a client
   */
  createClient() {
    this.clientService
      .createClient(this.clientForm.value as FormClient)
      .subscribe({
        next: (res: any) => {
          this.toast.success({
            detail: 'Sucesso!',
            summary: 'Perfil do utilizador registado!',
            duration: 5000,
          });

          var returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');

          if (returnUrl) {
            this.router.navigate(['/login'], {
              queryParams: { returnUrl: returnUrl },
            });
          } else {
            this.router.navigate(['/login']);
          }
        },
        error: (error: any) => {
          if (error.status == 422) {
            this.error1 = true;
          } else if (error.status == 409) {
            this.error2 = true;
          } else {
            this.error3 = true;
          }
        },
      });
  }

  //errors alert management
  closeError(num: number) {
    if (num == 1) {
      this.error1 = false;
    } else if (num == 2) {
      this.error2 = false;
    } else if (num == 3) {
      this.error3 = false;
    }
  }
}
