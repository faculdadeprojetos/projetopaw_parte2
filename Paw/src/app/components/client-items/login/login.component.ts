import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  loading: boolean = false;
  errorLoginFailed: boolean = false;

  ngOnInit(): void {
  }

  /**
   * Attempts to login
   */
  login(email: string, pass: string) {
    this.loading = true;
    this.auth.login(email, pass).subscribe({next: (res) => {
      localStorage.setItem('currentUser', JSON.stringify(res));
      this.auth.loggedIn();

      let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');

      if (returnUrl) {
        if (returnUrl.includes('?')) {
          location.href = returnUrl;
        }
        this.router.navigateByUrl(returnUrl);
      }
    }, error: () => {
      this.loading = false;
      this.errorLoginFailed = true;
    }});
  }

  closeErrorLoginFailed() {
    this.errorLoginFailed = false;
  }

  register() {
    var returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');

    this.router.navigate(['/clients/registration'], {
      queryParams: { returnUrl: returnUrl },
    });
  }
}
