import { Component } from '@angular/core';
import { Sale } from 'src/app/models/sale';
import { SaleServiceService } from 'src/app/services/sale.service.service';
import { PersonalInformationComponent } from '../personal-information/personal-information.component';
import { SearchBarService } from 'src/app/services/searchbar/search-bar.service';

@Component({
  selector: 'app-see-sales',
  templateUrl: './see-sales.component.html',
  styleUrls: ['./see-sales.component.css'],
})
export class SeeSalesComponent {
  salesByClient: Sale[] = [];
  p: number = 0;

  eventName!: string;

  searchText: string = '';

  constructor(
    private saleService: SaleServiceService,
    private personal: PersonalInformationComponent,
    private searchBar: SearchBarService
  ) {}

  ngOnInit(): void {
    this.getSales();
    this.searchBar.searchText.subscribe((searchText) => {
      this.searchText = searchText;
    });
  }

  getSales() {
    this.saleService.getSalesByClient().subscribe((sales: Sale[]) => {
      this.salesByClient = sales;
    });
  }

  closeSalesInformation() {
    this.personal.isWatchingSales = false;
  }

  generateQRCode(sale: Sale): string {
    // Get the first ticket from the sale
    let ticket0 = sale.tickets[0];

    // Turn the string ticket0 into an object
    let parsedTicket = JSON.parse(ticket0 as unknown as string);

    // Get the name of the event from the ticket object
    var ticketName = parsedTicket.name;

    // Add that name to the eventName variable so it's shown under the QR code
    this.eventName = ticketName;

    // Create the data that will be encoded in the QR code
    var qrData = {
      eventName: ticketName,
      salesId: sale.salesId,
      clientId: sale.client.clientId,
      name: sale.client.name,
      nif: sale.client.nif,
    };

    var qrCode = JSON.stringify(qrData, null, 4);
    return qrCode;
  }
}
