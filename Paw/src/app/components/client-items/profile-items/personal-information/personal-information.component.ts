import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from 'src/app/models/client';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ClientRestService } from 'src/app/services/client-rest.service';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.css'],
})
export class PersonalInformationComponent implements OnInit {
  @Input() client!: Client;

  isEditing?: boolean = false;
  isWatchingSales?: boolean = false;

  constructor(
    private auth: AuthService,
    private clientService: ClientRestService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (!this.auth.isLoggedIn()) {
      this.router.navigate(['login']);
    } else {
      this.getCurrentClient();
    }
  }

  editProfile() {
    this.isEditing = true;
  }

  seeSales() {
    this.isWatchingSales = true;
  }

  getCurrentClient(): void {
    this.clientService.getClient().subscribe((client: Client) => {
      if (client == null) {
        this.router.navigate(['login']);
      } else {
        this.client = client;
      }
    });
  }
}
