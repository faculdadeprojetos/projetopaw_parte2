import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { Client } from 'src/app/models/client';
import { SaleTicket } from 'src/app/models/saleTickets';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ClientRestService } from 'src/app/services/client-rest.service';
import { SaleServiceService } from 'src/app/services/sale.service.service';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { PointsRestService } from 'src/app/services/points/points-rest.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
})
export class CheckoutComponent implements OnInit {
  saleTicket!: SaleTicket;
  unfinishedSaleTicket!: any;

  clientInfo!: Client;
  clientNif!: number;
  hasAppliedDiscount!: boolean;

  isClientLoggedIn: boolean = false;

  discountPer100Points!: number;
  totalPriceAfterDiscount!: number;
  roughEstimatedPointsAfterDiscountIsApplied!: number;

  constructor(
    private saleService: SaleServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private clientService: ClientRestService,
    private pointSystemService: PointsRestService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.hasAppliedDiscount = false;
    this.isClientLoggedIn = this.authService.isUserLoggedMessenger.getValue();

    this.getUnfinishedSaleTicket();
    this.getDiscountPer100Points();

    if (this.isClientLoggedIn) {
      this.searchClientInfo();
    }
  }

  searchClientInfo(): void {
    if (this.isClientLoggedIn) {
      this.clientService.getClient().subscribe((client: Client) => {
        // Assign the client info to the variable
        this.clientInfo = client;
        this.clientNif = client.personalInformation.nif;

        // Store the points in a variable to support the following calculations
        let actualPointsDivisibleBy100 =
          this.clientInfo.loyaltySystem.actualPoints -
          (this.clientInfo.loyaltySystem.actualPoints % 100);

        // Calculate the total price after the discount is applied
        this.totalPriceAfterDiscount =
          this.unfinishedSaleTicket.totalPrice -
          (actualPointsDivisibleBy100 / 100) * this.discountPer100Points;

        // If the total price after the discount is applied is negative, set it to 0
        if (this.totalPriceAfterDiscount < 0) {
          this.totalPriceAfterDiscount = 0;
        }

        // Calculate the points that the client will have after the discount is applied
        // Without taking into account the points that the client will get from the purchase
        this.roughEstimatedPointsAfterDiscountIsApplied =
          this.clientInfo.loyaltySystem.actualPoints % 100;
      });
    } else {
      this.router.navigate(['/login'], {
        queryParams: { returnUrl: this.route.snapshot.url },
      });
    }
  }

  openDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      message: 'Do you confirm that this is your information?',
      clientInfo: this.clientInfo,
    };

    const dialogRef = this.dialog.open(
      ConfirmationDialogComponent,
      dialogConfig
    );

    dialogRef.afterClosed().subscribe(() => {
      // Get the value of the checkbox in the confirmation dialog
      this.hasAppliedDiscount = this.saleService.getAppliedDiscount();
      this.confirmCheckout();
    });
  }

  getUnfinishedSaleTicket(): void {
    if (this.saleService.accessedFromEventDetails) {
      this.saleService.unfinishedSaleTicket.subscribe(
        (unfinishedSaleTicket: any) => {
          this.unfinishedSaleTicket = unfinishedSaleTicket;
          this.saleService.accessedFromEventDetails = false;
          localStorage.setItem(
            'unfinishedSaleTicket',
            JSON.stringify(unfinishedSaleTicket)
          );
        }
      );
    } else {
      this.unfinishedSaleTicket = JSON.parse(
        localStorage.getItem('unfinishedSaleTicket') || 'null'
      );
    }
  }

  confirmCheckout(): void {
    this.saleTicket = new SaleTicket(
      this.unfinishedSaleTicket.vipTickets,
      this.unfinishedSaleTicket.familyTickets,
      this.unfinishedSaleTicket.normalTickets,
      this.unfinishedSaleTicket.childTickets,
      this.unfinishedSaleTicket.totalTickets,
      this.unfinishedSaleTicket.eventName,
      this.clientNif,
      this.hasAppliedDiscount
    );

    this.saleService.registerSale(this.saleTicket).subscribe({
      next: () => {
        this.saleService.addUnfinishedSaleTicketToCheckout(
          this.unfinishedSaleTicket.eventName
        );
        localStorage.removeItem('unfinishedSaleTicket');
        this.unfinishedSaleTicket = undefined;
        this.router.navigate(['/checkout/after-checkout']);
      },
      error: () => {
        document.getElementById('error')!.removeAttribute('hidden');
      },
    });
  }

  getDiscountPer100Points(): void {
    this.pointSystemService.getDiscountPer100Points().subscribe((res: any) => {
      this.discountPer100Points = res.discountPer100Points;
    });
  }
}
