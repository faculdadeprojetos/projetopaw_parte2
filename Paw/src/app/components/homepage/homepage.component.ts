import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { EventRestService } from '../../services/event-rest.service';
import { SearchBarFilterPipe } from '../../shared/searchBarFilter.pipe';
import { Event } from '../../models/event';
import { SearchBarService } from 'src/app/services/searchbar/search-bar.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
  providers: [DatePipe],
})
export class HomepageComponent implements OnInit {
  events: Event[] = []; // Array to store events
  p: number = 0; // Current page for pagination

  listing: boolean; // Flag to determine if events are being listed or details are shown
  clickedEvent!: Event; // Currently clicked event

  searchText!: string; // Search text for search bar

  first5events: Event[] = [];
  static i: number = 0; // static index for the display picture

  currentImageIndex: number = 0; // Index of the current image being displayed

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private eventRestService: EventRestService,
    private search: SearchBarService
  ) {
    this.listing = true; // Set listing flag to true initially
  }

  ngOnInit(): void {
    this.getEvents(); // Call method to fetch events when component initializes
    this.search.searchText.subscribe((searchText: string) => {
      this.searchText = searchText; // Set search text
    });
  }

  // Fetches events from the eventRestService
  getEvents(): void {
    this.eventRestService.getEvents().subscribe((events: Event[]) => {
      this.events = events; // Assign fetched events to the 'events' array
      this.getClosestEventsToToday(); // Call method to get the closest 5 events to today
      sessionStorage.setItem('events', JSON.stringify(events)); // Store events in local storage
    });
  }

  getClosestEventsToToday(): void {
    this.eventRestService
      .getClosestEventsToToday()
      .subscribe((events: Event[]) => {
        this.first5events = events; // Assign fetched events to the 'events' array

        // After 1 milisecond, call the method to add the event listeners to the next and previous arrows
        // and the method to move the image slider automatically
        setTimeout(() => {
          this.addEventListeners();
          this.moveImageSliderAutomatically();
        }, 1);
      });
  }
  // Shows details of a specific event
  showDetails(event: Event): void {
    this.clickedEvent = event; // Set the clicked event
    this.listing = false; // Set listing flag to false to show event details
  }

  // Add eventListener to the next and previous arrows
  addEventListeners(): void {
    document.getElementById('rightArrow')?.addEventListener('click', () => {
      this.slideToNextImage();
    });

    document.getElementById('leftArrow')?.addEventListener('click', () => {
      this.slideToPreviousImage();
    });
  }

  slideToNextImage(): void {
    // Increase the display picture index by 1
    HomepageComponent.i = HomepageComponent.i + 1;
    // If we are at the end of the image queue, set the index back to 0
    if (HomepageComponent.i == this.first5events.length) {
      HomepageComponent.i = 0;
    }

    // Set current image and previous image within the banner container
    const currentImg = $('.sliderContainer img').eq(HomepageComponent.i);
    const prevImg = $('.sliderContainer img').eq(HomepageComponent.i - 1);
    this.currentImageIndex = HomepageComponent.i;
    // Call function to animate the rotation of the images to the right
    animateImage(prevImg, currentImg);
  }

  slideToPreviousImage(): void {
    // If we are at the beginning of the image queue, set the previous image to the first image and the current image to the last image of the queue
    if (HomepageComponent.i === 0) {
      const prevImg = $('.sliderContainer img').eq(0);
      HomepageComponent.i = this.first5events.length - 1;
      const currentImg = $('.sliderContainer img').eq(HomepageComponent.i);
      // Call function to animate the rotation of the images to the left
      animateImageLeft(prevImg, currentImg);
    } else {
      // Decrease the display picture index by 1
      HomepageComponent.i = HomepageComponent.i - 1;
      // Set current and previous images within the banner container
      const currentImg = $('.sliderContainer img').eq(HomepageComponent.i);
      const prevImg = $('.sliderContainer img').eq(HomepageComponent.i + 1);
      this.currentImageIndex = HomepageComponent.i;
      // Call function to animate the rotation of the images to the left
      animateImageLeft(prevImg, currentImg);
    }
  }

  moveImageSliderAutomatically(): void {
    setInterval(function () {
      const nextArrow = $('.next');
      nextArrow.trigger('click');
    }, 2500);
  }

  getDaysLeft(eventDate: any): number {
    const currentDate = new Date();
    const convertedEventDate = new Date(eventDate);
    const timeDifference = convertedEventDate.getTime() - currentDate.getTime();
    const daysLeft = Math.ceil(timeDifference / (1000 * 3600 * 24));
    return daysLeft;
  }
}

function animateImageLeft(
  prevImg: JQuery<HTMLElement>,
  currentImg: JQuery<HTMLElement>
) {
  //move the image to be displayed off the visible container to the right
  currentImg.css({ left: '-100%' });
  //slide the image to be displayed from off the container onto the visible container to make it slide from the right to left
  currentImg.animate({ left: '0%' }, 1000);
  //slide the previous image off the container from right to left
  prevImg.animate({ left: '100%' }, 1000);
}
//function to animate the rotation of the images to the right
function animateImage(
  prevImg: JQuery<HTMLElement>,
  currentImg: JQuery<HTMLElement>
) {
  //move the image to be displayed off the container to the left
  currentImg.css({ left: '100%' });
  //slide the image to be displayed from off the container onto the container to make it slide from left to right
  currentImg.animate({ left: '0%' }, 1000);
  //slide the image from on the container to off the container to make it slide from left to right
  prevImg.animate({ left: '-100%' }, 1000);
}
