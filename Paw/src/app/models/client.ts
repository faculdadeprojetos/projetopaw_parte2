interface personalInformation {
  clientId: number;
  name: string;
  gender: string;
  dob: string;
  cellPhone: number;
  email: string;
  password: string;
  address: string;
  city: string;
  postalCode: string;
  nif: number;
}

interface loyaltySystem {
  creationMemberDate: string;
  ticketsPurchased: number;
  totalMoneyPurchases: number;
  actualPoints: number;
}

export class Client {
  constructor(
    public personalInformation: personalInformation,
    public loyaltySystem: loyaltySystem
  ) {}
}
