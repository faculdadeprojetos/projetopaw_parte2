interface schedule {
  start: string;
  end: string;
}
export interface Event {
  eventId: number;
  name: string;
  description: string;
  type: string;
  date: Date;
  schedule: schedule;
  location: number;
  classification: string;
  seatsAvailable: number;
  finished: boolean;
  updated_at: Date;
  imageUrl: string;
}
